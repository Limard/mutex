package mutex

import (
	"syscall"
	"unsafe"

	"bitbucket.org/Limard/golang/convert"
)

var (
	kernelDll        = syscall.NewLazyDLL("Kernel32.dll")
	procCreateMutexA = kernelDll.NewProc("CreateMutexA")
	procOpenMutexA   = kernelDll.NewProc("OpenMutexA")
	procCloseHandle  = kernelDll.NewProc("CloseHandle")
)

const (
	STANDARD_RIGHTS_REQUIRED = 0x000F0000
	SYNCHRONIZE              = 0x00100000
	MUTANT_QUERY_STATE       = 0x0001
	MUTEX_ALL_ACCESS         = STANDARD_RIGHTS_REQUIRED | SYNCHRONIZE | MUTANT_QUERY_STATE
)

// OpenOrCreateNamedMutex is used for ...
func OpenOrCreateNamedMutex(name string) (bool, error) {
	handle, _, err := procOpenMutexA.Call(uintptr(MUTEX_ALL_ACCESS), uintptr(1),
		uintptr(unsafe.Pointer(syscall.StringBytePtr(name))))

	if handle != 0 {
		return false, err
	}

	_, _, err = procCreateMutexA.Call(uintptr(0), uintptr(1),
		uintptr(unsafe.Pointer(syscall.StringBytePtr(name))))

	return true, err
}

// TryOpenNamedMutex ...
func TryOpenNamedMutex(name string) (bool, error) {
	handle, _, err := procOpenMutexA.Call(uintptr(MUTEX_ALL_ACCESS), uintptr(1), convert.Str2ptr(name))

	if handle != 0 {
		procCloseHandle.Call(handle)
		return true, nil
	}

	return false, err
}
